package app.lab6dpapp;

import com.sun.management.OperatingSystemMXBean;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

public class Controller {
    public String info;
    private Stage stageRegister;
    private Stage stageUser;
    private Stage stageAdmin;
    private int attempts;
    private final String DataBase = "data.txt"; //"src/main/resources/data/data.txt";
    private final String EncryptedDataBase = "data.enc"; //"src/main/resources/data/data.txt";
    private static String passphrase;
    private static User user;
    private static String log;
    private static String pas;
    private static Boolean res;
    private boolean isAdmin;
    private static ArrayList<User> ArrayUsers;
    public static ArrayList<User> GetArrUsr(){
        return ArrayUsers;
    }

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button closeButton;
    @FXML
    private TextField TextFieldPassphrase;
    @FXML
    private Button okButton;
    @FXML
    private Label LabelPassphrase;


    @FXML
    private Button SignUp;
    @FXML
    private Button SignUpS;
    @FXML
    private Button ExitButton;

    @FXML
    private Button Login;
    @FXML
    private Button NotOK;

    @FXML
    private PasswordField UserPassword;
    @FXML
    private PasswordField UserPassword1;
    @FXML
    private PasswordField UserPassword2;
    @FXML
    private PasswordField OldPassField;
    @FXML
    private PasswordField NewPassField1;
    @FXML
    private PasswordField NewPassField2;

    @FXML
    private Label LabelLogin;
    @FXML
    private Label LabelLogin1;
    @FXML
    private Label LabelPass1;
    @FXML
    private Label LabelPass2;
    @FXML
    private Label LabelPass3;

    @FXML
    private TextField UserLogin;
    @FXML
    private TextField UserLogin1;

    @FXML
    private MenuBar MainMenuBar;
    @FXML
    private MenuBar AdminMenuBar;

    @FXML
    private MenuItem Settings;
    @FXML
    private MenuItem LogOut;
    @FXML
    private MenuItem About;

    @FXML
    private Button ChangeButton;
    @FXML
    private Button CancelButton;

    @FXML
    private Label LabelS1;
    @FXML
    private Label LabelS2;
    @FXML
    private Label LabelS3;
    @FXML
    private Label LabelS0;
    @FXML
    private Label ErrorLabel1;
    @FXML
    private Label ErrorLabel2;
    @FXML
    private Label ErrorLabel3;
    @FXML
    private Label ErrorLabel4;
    @FXML
    private Label SucLabel1;

    @FXML
    private Menu AdminMenu;
    @FXML
    private MenuItem Admin;

    @FXML
    private Menu MainMenu;

    @FXML
    void OnActionDecryption(ActionEvent event) {
        passphrase = TextFieldPassphrase.getText();
        AdminController.SetPass(passphrase);
        FileEncryption fe = new FileEncryption(passphrase);

        File encryptedFile = new File(EncryptedDataBase);
        File dataBase = new File(DataBase);
        if (!encryptedFile.exists()) {
            LabelPassphrase.setTextFill(Color.valueOf("GREY"));
            LabelPassphrase.setAlignment(Pos.TOP_CENTER);
            LabelPassphrase.setText("This is the first launch of the application, this passphrase will be used to encrypt the database");

            try {
                if (!dataBase.createNewFile()){
                    System.out.println("file..");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try (FileWriter fileWriter = new FileWriter(DataBase)) {
                String text = "ADMIN  0 false\n";
                fileWriter.write(text);
            }
            catch(IOException e){
                e.printStackTrace();
            }
            fe.Encrypt(DataBase, EncryptedDataBase);
            System.out.println("new base delete: ".concat(Boolean.toString(dataBase.delete())));
        }

        fe.Decrypt(EncryptedDataBase);
        if (GetUser("ADMIN").GetLogin().equals("")){
            LabelPassphrase.setTextFill(Color.valueOf("RED"));
            LabelPassphrase.setAlignment(Pos.TOP_CENTER);
            LabelPassphrase.setText("Wrong passphrase!");
        } else {
            LabelPassphrase.setTextFill(Color.valueOf("GREEN"));
            LabelPassphrase.setAlignment(Pos.TOP_CENTER);
            LabelPassphrase.setText("Success!");

            try {
                MainApplication.startLoginPage();
                MainApplication.changeVisiblePassStage(false);
                ArrayUsers = GetArrayUsers();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void OnActionAdmin(ActionEvent event) throws IOException {
        if (!(log.equals("ADMIN"))){
            this.Admin.setDisable(true);
            this.AdminMenu.setDisable(true);
        } else {
            this.stageAdmin = new Stage();
            this.stageAdmin = MainApplication.startPage(stageAdmin, "admin-page.fxml", "Admin page");
        }
    }

    @FXML
    void About(ActionEvent event) throws IOException {
        Stage newAbout = MainApplication.startPage(new Stage(), "About.fxml", "About");
    }

    @FXML
    void OnActionCancel(ActionEvent event) {
        LabelS0.setVisible(false);
        LabelS1.setVisible(false);
        LabelS2.setVisible(false);
        LabelS3.setVisible(false);

        OldPassField.setVisible(false);
        NewPassField1.setVisible(false);
        NewPassField2.setVisible(false);

        ChangeButton.setVisible(false);
        CancelButton.setVisible(false);
    }

    @FXML
    void OnActionChange(ActionEvent event) throws IOException {
        SucLabel1.setText("");
        ErrorLabel1.setText("");
        ErrorLabel2.setText("");
        ErrorLabel3.setText("");
        ErrorLabel4.setText("");
        String old_password = OldPassField.getText();

        /*хэширование здесь*/
        Hashing hash = new Hashing(old_password);
        old_password = hash.Hash();
        /*хэширование здесь*/

        if (!old_password.equals(pas)){
            ErrorLabel1.setTextFill(Paint.valueOf("RED"));
            ErrorLabel1.setText("Incorrect password. Try again");
            OldPassField.setText("");
        } else{
            String new_pass = NewPassField1.getText();
            String rep_pass = NewPassField2.getText();

            if (!new_pass.equals("")){
                ErrorLabel2.setTextFill(Paint.valueOf("RED"));
                ErrorLabel2.setText("Invalid password");
            }

            if (IsCorrectPass(new_pass, res)) {
                if (rep_pass.equals("")) {
                    ErrorLabel3.setTextFill(Paint.valueOf("RED"));
                    ErrorLabel3.setText("Repeat new password");
                } else {
                    if (!new_pass.equals(rep_pass)) {
                        ErrorLabel3.setTextFill(Paint.valueOf("RED"));
                        ErrorLabel3.setText("Passwords don't look similar");
                    } else {
                        ErrorLabel1.setText("");
                        ErrorLabel2.setText("");
                        ErrorLabel3.setText("");
                        ErrorLabel4.setText("");

                        /*decrypt file here*/
                        FileEncryption fe = new FileEncryption(passphrase);
                        fe.Decrypt(EncryptedDataBase);
                        File dataBase = new File(DataBase);

                        StringBuilder sb = new StringBuilder();
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(DataBase)))) {
                            String line;
                            while ((line = br.readLine()) != null) {
                                String[] buf = line.split(" ");
                                if (buf[0].equals(log)) {
                                    /*хэширование здесь*/
                                    Hashing hash1 = new Hashing(new_pass);
                                    new_pass = hash1.Hash();
                                    /*хэширование здесь*/
                                    sb.append(line.replace(line, buf[0]+" "+new_pass+" "+buf[2]+" "+ buf[3])).append("\r\n");
                                } else {
                                    sb.append(line).append("\r\n");
                                }
                            }
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        try (FileWriter fileWriter = new FileWriter(DataBase)) {
                            fileWriter.write(sb.toString());
                            fileWriter.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        SucLabel1.setTextFill(Paint.valueOf("GREEN"));
                        SucLabel1.setAlignment(Pos.TOP_CENTER);
                        SucLabel1.setText("Success!");


                        fe.Encrypt(DataBase, EncryptedDataBase);
                        System.out.println("change delete: ".concat(Boolean.toString(dataBase.delete())));
                    }
                }
            } else {
                ErrorLabel2.setTextFill(Paint.valueOf("RED"));
                ErrorLabel4.setTextFill(Paint.valueOf("RED"));
                ErrorLabel2.setText("Invalid password");
                ErrorLabel4.setText("Password must not contain date or ' '!");
            }
        }
    }

    @FXML
    void OnActionLogin(ActionEvent event) throws IOException {
        this.isAdmin = false;
        user = new User();
        String login;
        String password;

        login = UserLogin.getText();
        password = UserPassword.getText();

        if (!login.equals("")) {
            User checkUser = GetUser(login);

            //System.out.println(checkUser.GetPassword());

            if (checkUser.GetLogin().equals("")) {
                LabelLogin.setTextFill(Paint.valueOf("RED"));
                LabelLogin.setText("Invalid login!");
            } else {
                if (checkUser.GetPassword().equals("")){
                    this.stageRegister = new Stage();
                    //stageRegister = MainApplication.startPage(stageRegister, "sign-up.fxml", "Sign up page");
                    FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("sign-up.fxml"));
                    Scene scene = new Scene(fxmlLoader.load());
                    this.stageRegister.setTitle("Sign up page");
                    this.stageRegister.setScene(scene);
                    this.stageRegister.setResizable(false);
                    this.stageRegister.show();
                } else {
                    /*здесь*/
                    Hashing hash = new Hashing(password);
                    password = hash.Hash();
                    /*здесь*/
                    if (checkUser.GetPassword().equals(password)) {
                        if (!checkUser.GetStatus().equals("Blocked")) {
                            isAdmin = checkUser.GetStatus().equals("Admin");

                            LabelLogin.setTextFill(Paint.valueOf("GREY"));
                            LabelLogin.setText("Success!");
                            UserPassword.setText("");

                            user.SetUser(checkUser.Login, checkUser.Password, checkUser.Status, checkUser.Restriction);

                            if (isAdmin) {
                                this.stageAdmin = new Stage();
                                MainApplication.startPage(stageAdmin, "admin-page.fxml", "Admin page");
                            } else {
                                this.stageUser = new Stage();
                                MainApplication.startPage(stageUser, "main-page.fxml", "Main page");
                            }

                            log = checkUser.Login;
                            pas = checkUser.Password;
                            res = checkUser.Restriction;
                            //System.out.println(log+pas);

                            MainApplication.changeVisibleLoginStage(false);
                            LabelLogin.setText("");

                        } else {
                            LabelLogin.setTextFill(Paint.valueOf("RED"));
                            LabelLogin.setText("This account is blocked");
                        }
                    } else {
                        LabelLogin.setTextFill(Paint.valueOf("RED"));
                        LabelLogin.setText("Invalid password!");
                        attempts += 1;
                        if (attempts == 3) {
                            File file = new File(DataBase);
                            System.out.println("attempt delete: ".concat(Boolean.toString(file.delete())));
                            System.exit(0);
                        }
                    }
                }
            }
        } else {
            LabelLogin.setTextFill(Paint.valueOf("RED"));
            LabelLogin.setText("Invalid login!");
        }
    }

    @FXML
    void OnActionExit(ActionEvent event) throws IOException {
        File file = new File(DataBase);
        System.out.println("exit delete: ".concat(Boolean.toString(file.delete())));
        System.exit(0);
    }

    @FXML
    void OnActionRegister(ActionEvent event) throws IOException {
        stageRegister = new Stage();
        stageRegister = MainApplication.startPage(stageRegister, "sign-up.fxml", "Sign up page");

    }

    @FXML
    void OnActionRegisterUser(ActionEvent event) throws IOException { //
        String new_login = UserLogin1.getText();
        String new_password = UserPassword1.getText();
        String rep_password = UserPassword2.getText();
        User newUser = new User();

        boolean flag = false;
        boolean isUser = false;
        boolean isCorrPass = false;

        LabelLogin.setText("");
        LabelLogin1.setText("");
        LabelPass1.setText("");
        LabelPass2.setText("");
        LabelPass3.setText("");

        if (new_login.equals("")){
            LabelLogin.setTextFill(Paint.valueOf("RED"));
            LabelLogin.setText("Invalid login");
        } else {
            for (User arrayUser : ArrayUsers){
                if (arrayUser.GetLogin().equals(new_login)){
                    isUser = true;
                    newUser = arrayUser;
                    break;
                }
            }
            if (!isUser){
                LabelLogin.setTextFill(Paint.valueOf("RED"));
                LabelLogin.setText("There is no such user!");
            }
        }
        if (isUser) {
            if (!newUser.GetPassword().equals("")){
                LabelLogin.setTextFill(Paint.valueOf("RED"));
                LabelLogin.setText("Invalid login!");
            } else {
                if (new_password.equals("")){
                    LabelPass1.setTextFill(Paint.valueOf("RED"));
                    LabelPass1.setText("This field must not be empty!");
                    flag = true;
                } else {
                    if (rep_password.equals("")) {
                        LabelPass2.setTextFill(Paint.valueOf("RED"));
                        LabelPass2.setText("This field must not be empty!");
                        flag = true;
                    }
                    {

                        isCorrPass = IsCorrectPass(new_password, newUser.GetRestriction());
                        //System.out.println(isCorrPass);
                        if (!isCorrPass) {
                            LabelPass1.setTextFill(Paint.valueOf("RED"));
                            LabelPass1.setText("Invalid password!");
                            LabelPass3.setTextFill(Paint.valueOf("RED"));
                            LabelPass3.setAlignment(Pos.TOP_CENTER);
                            LabelPass3.setText("Dates cannot be used in password!");
                        }

                        if (isCorrPass) {
                            if (!new_password.equals(rep_password)) {
                                LabelPass2.setTextFill(Paint.valueOf("RED"));
                                LabelPass2.setText("Passwords don't look similar");
                            } else {
                                StringBuilder sb = new StringBuilder();
                                /*decrypt file here*/
                                FileEncryption fe = new FileEncryption(passphrase);
                                fe.Decrypt(EncryptedDataBase);
                                File dataBase = new File(DataBase);

                                try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(DataBase)))) {
                                    String line;
                                    while ((line = br.readLine()) != null) {
                                        String[] buf = line.split(" ");
                                        if (buf[0].equals(new_login)) {
                                            /*здесь*/
                                            Hashing hash = new Hashing(new_password);
                                            new_password = hash.Hash();
                                            /*здесь*/
                                            sb.append(line.replace(line, buf[0] + " " + new_password + " " + buf[2] + " " + buf[3])).append("\r\n");
                                        } else {
                                            sb.append(line).append("\r\n");
                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                try (FileWriter fileWriter = new FileWriter(DataBase)) {
                                    fileWriter.write(sb.toString());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                fe.Encrypt(DataBase, EncryptedDataBase);
                                System.out.println("register delete: ".concat(Boolean.toString(dataBase.delete())));

                                LabelPass3.setTextFill(Paint.valueOf("GREEN"));
                                LabelPass3.setAlignment(Pos.TOP_CENTER);
                                LabelPass3.setText("Success!");

                                UserPassword1.setText("");
                                UserPassword2.setText("");
                                ArrayUsers = GetArrayUsers();
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean IsCorrectPassHelp (String pass) {
        //System.out.println(pass);
        if (pass.length() < 8){
            return true;
        }
        int count = 0;
        char sep = ' ';
        for (int i = 0; i < pass.length(); i++){
            if (Character.isDigit(pass.charAt(i))){
                count++;
            } else if (Character.isLetter(pass.charAt(i))) {
                return true;
            } else if (pass.charAt(i) == '.' || pass.charAt(i) == '/' || pass.charAt(i) == '-') {
                if (count % 2 == 0) {
                    if (sep == ' ') {
                        sep = pass.charAt(i);
                    } else if (sep != pass.charAt(i)){
                        return true;
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
            if (count >= 6){
                return false;
            }
        }
        return false;
    }

    private boolean IsCorrectPass(String pass, boolean restriction) {
        for (int i = 0; i < pass.length(); i++){
            if (restriction && Character.isDigit(pass.charAt(i))){
                boolean flag = IsCorrectPassHelp(pass.substring(i));
                if (!flag){
                    return false;
                }
            }
            if (pass.charAt(i) == ' ') {
                return false;
            }
        }
        return true;
    }

    @FXML
    void OnActionLogOut(ActionEvent event) {
        user = new User();
        MainApplication.changeVisibleLoginStage(true);

        Stage stage = (Stage) MainMenuBar.getScene().getWindow();
        stage.close();
        isAdmin = false;
    }

    @FXML
    void OnActionSettings(ActionEvent event) {
        LabelS0.setVisible(true);
        LabelS1.setVisible(true);
        LabelS2.setVisible(true);
        LabelS3.setVisible(true);

        OldPassField.setVisible(true);
        NewPassField1.setVisible(true);
        NewPassField2.setVisible(true);

        ChangeButton.setVisible(true);
        CancelButton.setVisible(true);
    }

    @FXML
    void OnActionClose(ActionEvent event) throws IOException {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    @FXML
    void initialize() {
        assert Login != null : "fx:id=\"Login\" was not injected: check your FXML file 'view.fxml'.";
        assert NotOK != null : "fx:id=\"NotOK\" was not injected: check your FXML file 'view.fxml'.";
        assert UserPassword != null : "fx:id=\"UserPassword\" was not injected: check your FXML file 'view.fxml'.";
        assert LabelLogin != null : "fx:id=\"LabelLogin\" was not injected: check your FXML file 'view.fxml'.";
        assert UserLogin != null : "fx:id=\"UserLogin\" was not injected: check your FXML file 'view.fxml'.";
        assert SignUp != null : "fx:id=\"SignUp\" was not injected: check your FXML file 'view.fxml'.";

        assert SignUpS != null : "fx:id=\"SignUp\" was not injected: check your FXML file 'view.fxml'.";
        assert closeButton != null : "fx:id=\"closeButton\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert UserLogin1 != null : "fx:id=\"UserLogin1\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert LabelLogin1 != null : "fx:id=\"LabelLogin1\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert UserPassword1 != null : "fx:id=\"UserPassword1\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert UserPassword2 != null : "fx:id=\"UserPassword2\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert LabelPass1 != null : "fx:id=\"LabelPass1\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert LabelPass2 != null : "fx:id=\"LabelPass2\" was not injected: check your FXML file 'sign-up.fxml'.";
        assert LabelPass3 != null : "fx:id=\"LabelPass3\" was not injected: check your FXML file 'sign-up.fxml'.";

        assert MainMenuBar != null : "fx:id=\"MainMenuBar\" was not injected: check your FXML file 'main-page.fxml'.";
        assert MainMenu != null : "fx:id=\"MainMenu\" was not injected: check your FXML file 'main-page.fxml'.";
        assert Settings != null : "fx:id=\"Settings\" was not injected: check your FXML file 'main-page.fxml'.";
        assert LogOut != null : "fx:id=\"LogOut\" was not injected: check your FXML file 'main-page.fxml'.";
        assert About != null : "fx:id=\"About\" was not injected: check your FXML file 'main-page.fxml'.";
        assert AdminMenu != null : "fx:id=\"AdminMenu\" was not injected: check your FXML file 'main-page.fxml'.";
        assert Admin != null : "fx:id=\"Admin\" was not injected: check your FXML file 'main-page.fxml'.";
        assert LabelS1 != null : "fx:id=\"LabelS1\" was not injected: check your FXML file 'main-page.fxml'.";
        assert LabelS2 != null : "fx:id=\"LabelS2\" was not injected: check your FXML file 'main-page.fxml'.";
        assert LabelS3 != null : "fx:id=\"LabelS3\" was not injected: check your FXML file 'main-page.fxml'.";
        assert LabelS0 != null : "fx:id=\"LabelS0\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ChangeButton != null : "fx:id=\"ChangeButton\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ErrorLabel1 != null : "fx:id=\"ErrorLabel1\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ErrorLabel2 != null : "fx:id=\"ErrorLabel2\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ErrorLabel3 != null : "fx:id=\"ErrorLabel3\" was not injected: check your FXML file 'main-page.fxml'.";
        assert OldPassField != null : "fx:id=\"OldPassField\" was not injected: check your FXML file 'main-page.fxml'.";
        assert NewPassField1 != null : "fx:id=\"NewPassField1\" was not injected: check your FXML file 'main-page.fxml'.";
        assert NewPassField2 != null : "fx:id=\"NewPassField2\" was not injected: check your FXML file 'main-page.fxml'.";
        assert SucLabel1 != null : "fx:id=\"SucLabel1\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ErrorLabel4 != null : "fx:id=\"ErrorLabel4\" was not injected: check your FXML file 'main-page.fxml'.";
        assert ExitButton != null : "fx:id=\"ExitButton\" was not injected: check your FXML file 'main-page.fxml'.";
        assert CancelButton != null : "fx:id=\"CancelButton\" was not injected: check your FXML file 'main-page.fxml'.";

        assert TextFieldPassphrase != null : "fx:id=\"TextFieldPassphrase\" was not injected: check your FXML file 'file-decryption.fxml'.";
        assert okButton != null : "fx:id=\"okButton\" was not injected: check your FXML file 'file-decryption.fxml'.";
        assert LabelPassphrase != null : "fx:id=\"LabelPassphrase\" was not injected: check your FXML file 'file-decryption.fxml'.";

        this.attempts = 0;
        //ArrayUsers = GetArrayUsers();
        user = new User();

        /*for (User arrayUser : ArrayUsers){
            System.out.println(arrayUser.GetLogin());
        }*/
    }

    public static class User {
        private String Login;
        private String Password;
        private int Status;
        private boolean Restriction;

        public User(){
            this.Login = "";
            this.Password = "";
            this.Status = 10;
            this.Restriction = false;
        }

        public User(String new_login, String new_password, int new_status, Boolean new_restriction){
            this.Login = new_login;
            this.Password = new_password;
            this.Status = new_status;
            if (new_login.equals("ADMIN")){
                this.Status = 0;
            }
            if (new_status == 0 && !new_login.equals("ADMIN")){
                this.Status = 10;
            }
            this.Restriction = new_restriction;
        }

        public void SetUser(String new_login, String new_password, int new_status, Boolean new_restriction){
            this.Login = new_login;
            this.Password = new_password;
            this.Status = new_status;
            if (new_login.equals("ADMIN")){
                this.Status = 0;
            }
            if (new_status == 0 && !new_login.equals("ADMIN")){
                this.Status = 10;
            }
            this.Restriction = new_restriction;
        }

        public void ChangeStatus(int s){
            this.Status = s;
        }

        public void ChangeRestriction(boolean r){
            this.Restriction = r;
        }

        public String GetLogin(){
            return this.Login;
        }

        public String GetPassword(){
            return this.Password;
        }

        public int GetIntStatus() {
            return this.Status;
        }

        public String GetStatus() {
            if (this.Status == 0){
                return "Admin";
            } else if (this.Status == 1){
                return "Blocked";
            } else if (this.Status == 2){
                return "Limited";
            }
            return "";
        }

        public Boolean GetRestriction() { return Restriction; }
    }

    public ArrayList<User> GetArrayUsers() {
        ArrayList<User> arrayUsers = new ArrayList<>();

        /*decrypt file here*/
        FileEncryption fe = new FileEncryption(passphrase);
        fe.Decrypt(EncryptedDataBase);
        File dataBase = new File(DataBase);

        System.out.println(passphrase);

        try {
            FileReader fr = new FileReader(dataBase);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            while (line != null) {
                User ArrayUser = new User();
                String[] bufer = line.split(" ");
                if (bufer.length == 4) {
                    ArrayUser.SetUser(bufer[0], bufer[1], Integer.parseInt(bufer[2]), Boolean.valueOf(bufer[3]));
                } else {
                    if (bufer.length == 2) {
                        ArrayUser.SetUser(bufer[0], bufer[1], 10, false);
                    }
                }
                arrayUsers.add(ArrayUser);
                line = reader.readLine();
            }
            reader.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("get array delete: ".concat(Boolean.toString(dataBase.delete())));
        AdminController.SetAU(arrayUsers);
        return arrayUsers;
    }

    private User GetUser(String login) {
        ArrayUsers = GetArrayUsers();
        for (User arrayUser : ArrayUsers) {
            if (login.equals(arrayUser.GetLogin())) {
                return arrayUser;
            }
        }

        return new User();
    }

    public static boolean GetUserForAdmin(String login) {
        for (User arrayUser : ArrayUsers) {
            if (login.equals(arrayUser.GetLogin())) {
                return true;
            }
        }
        return false;
    }

}