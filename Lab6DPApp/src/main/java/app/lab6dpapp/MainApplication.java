package app.lab6dpapp;

import com.sun.management.OperatingSystemMXBean;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;

public class MainApplication extends Application {
    private static Stage loginStage;
    private static Stage passStage;
    private static Stage checkStage;
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("exit-error.fxml")); //view.fxml
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Ошибка установки");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        checkStage = stage;
    }

    public static void startLoginPage() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("view.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Login page");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        loginStage = stage;
    }

    public static void startPassPage() throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("file-decryption.fxml")); //view.fxml
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("Decryption page");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        passStage = stage;
    }

    public static void changeVisibleLoginStage(boolean visibility){
        if (visibility && !loginStage.isShowing()){
            loginStage.show();
        } else {
            if (!visibility && loginStage.isShowing()){
                loginStage.hide();
            }
        }
    }

    public static void changeVisiblePassStage(boolean visibility){
        if (visibility && !passStage.isShowing()){
            passStage.show();
        } else {
            if (!visibility && passStage.isShowing()){
                passStage.hide();
            }
        }
    }

    public static void changeVisibleCheckStage(boolean visibility){
        if (visibility && !checkStage.isShowing()){
            checkStage.show();
        } else {
            if (!visibility && checkStage.isShowing()){
                checkStage.hide();
            }
        }
    }

    public static Stage startPage(Stage stage, String name, String title) throws  IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource(name));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
        return stage;
    }

    public static void main(String[] args) {
        launch();
    }
}