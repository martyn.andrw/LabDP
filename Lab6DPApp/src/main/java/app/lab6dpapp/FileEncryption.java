package app.lab6dpapp;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
//import oracle.security.crypto.core.MD4;

public class FileEncryption {
    private final String passphrase;
    private static final String ALGORITHM = "DES";
    private static final String TRANSFORMATION = "DES/CFB8/NoPadding";

    FileEncryption (String newPassphrase){
        passphrase = newPassphrase;
    }

    public void Encrypt(String fileName, String encryptedFileName){
        try {
            MD4 md4 = new MD4();
            md4.update(passphrase.getBytes(StandardCharsets.UTF_8));
            DESKeySpec secretKey = new DESKeySpec(md4.digest());

            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
            SecretKey secureKey = keyFactory.generateSecret(secretKey);

            //System.out.println(new String(secureKey.getEncoded()));

            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.ENCRYPT_MODE, secureKey);

            byte[] iv = cipher.getIV();

            /*Чтение файла*/
            String result = "";
            File file = new File(fileName);
            if (!file.exists()) {
                System.out.println("The file doesn't exist");
            } else {
                try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                    while (reader.ready()) {
                        result = result.concat(reader.readLine());
                        result = result.concat("\n");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("Encrypt delete: ".concat(Boolean.toString(file.delete())));

                /*Шифрование файла*/
                try (FileOutputStream fileOut = new FileOutputStream(encryptedFileName);
                     CipherOutputStream cipherOut = new CipherOutputStream(fileOut, cipher)) {
                    fileOut.write(iv);
                    cipherOut.write(result.getBytes());
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    void Decrypt(String fileName) {
        String content = "";

        try (FileInputStream fileIn = new FileInputStream(fileName)) {
            MD4 md4 = new MD4();
            md4.update(passphrase.getBytes(StandardCharsets.UTF_8));
            DESKeySpec secretKey = new DESKeySpec(md4.digest());

            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
            SecretKey secureKey = keyFactory.generateSecret(secretKey);

            //System.out.println(new String(secureKey.getEncoded()));

            byte[] fileIv = new byte[8];
            fileIn.read(fileIv);

            Cipher cipher = Cipher.getInstance(TRANSFORMATION);
            cipher.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(fileIv));

            try (
                    CipherInputStream cipherIn = new CipherInputStream(fileIn, cipher);
                    InputStreamReader inputReader = new InputStreamReader(cipherIn);
                    BufferedReader reader = new BufferedReader(inputReader)
            ) {

                try (FileWriter fileWriter = new FileWriter("data.txt")) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        fileWriter.write(line.concat("\n"));
                    }
                    fileWriter.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
