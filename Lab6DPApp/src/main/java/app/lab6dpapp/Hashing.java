package app.lab6dpapp;

import java.util.ArrayList;
import java.util.Random;

public class Hashing {
    private int seed;
    private final int countChars;
    private final ArrayList<String> encryptionTable;
    private final String magicStr = "12345678";
    private final String alphabet = "!#$%&'()*+,-./01234567890:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ^_`abcdefghijklmnopqrstuvwxyz{|}~АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";

    private String shiftLeft(String magicStr){
        StringBuilder mStr = new StringBuilder();
        for (int i = 1; i < magicStr.length(); i++){
            mStr.append(magicStr.charAt(i));
        }
        mStr.append(magicStr.charAt(0));
        return mStr.toString();
    }

    Hashing(String password){
        seed = 0;
        for (char ch : password.toCharArray()){
            seed += ch;
        }
        seed = seed % 256;
        countChars = alphabet.length();

        encryptionTable = new ArrayList<>();

        encryptionTable.add(alphabet);
        String str = shiftLeft(alphabet);
        for (int i = 1; i < countChars; i++){
            encryptionTable.add(str);
            str = shiftLeft(str);
        }
    }

    public String Hash(){
        StringBuilder hash = new StringBuilder();
        StringBuilder key = new StringBuilder();

        Random random = new Random(seed);
        for (int i = 0; i < 8; i++){
            var rnd = random.nextInt(countChars);
            key.append(alphabet.charAt(rnd));
            //System.out.print(rnd + " ");
        }
        /*
         *System.out.println();
         *System.out.println(magicStr);
         *System.out.println(key.toString());
         *System.out.println(alphabet);
         */
        ArrayList<String> iters = new ArrayList<>();
        for (var str : encryptionTable){
            for (int i = 0; i < 8; i++) {
                if (str.charAt(0) == key.charAt(i)) {
                    iters.add(str);
                }
            }
        }
        /*for(var c : iters){
        *    System.out.println(c);
        }*/
        for(int i = 0; i < 8; i++){
            for (var str : encryptionTable){
                if (str.charAt(0) == key.charAt(i)){
                    hash.append(str.charAt(alphabet.indexOf(magicStr.charAt(i))));
                }
            }
        }

        return hash.toString();
    }

    public int GetSeed(){
        return seed;
    }
}
