package app.lab6dpapp;

import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.ResourceBundle;

import com.sun.management.OperatingSystemMXBean;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;

public class EndOfTime {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane AnchorInstallation;

    @FXML
    private Button ButtonContinue;

    @FXML
    private Label LabelHint;

    @FXML
    private TextField TextFieldLastName;

    @FXML
    private Button ButtonCheck;

    @FXML
    private AnchorPane AnchorInstallation1;

    @FXML
    private Button ButtonContinue1;

    @FXML
    private Label LabelError1;

    @FXML
    void OnActionExit(ActionEvent event) {
        System.exit(0);
    }

    private int counter;
    @FXML
    void OnActionCheck(ActionEvent event) {
        String lastName = TextFieldLastName.getText();
        if (!lastName.equals("Мартын")) {
            if (counter == 0) {
                LabelHint.setText("Имя раздела - это фамилия студента");
                counter++;
            } else if (counter >= 1) {
                LabelHint.setText("Фамилия студента: Мартын");
                counter++;
            }
        } else {
            getInfo();
            if (!checkSignature()) {
                AnchorInstallation.setVisible(false);
                AnchorInstallation1.setVisible(true);
                LabelError1.setText("Ошибка при проверки подписи!");
                LabelError1.setTextFill(Paint.valueOf("Tomato"));
                LabelError1.setAlignment(Pos.TOP_CENTER);
            } else {
                try {
                    MainApplication.changeVisibleCheckStage(false);
                    MainApplication.startPassPage();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    void onActionTextField(InputMethodEvent event) {

    }

    @FXML
    void initialize() {
        assert AnchorInstallation != null : "fx:id=\"AnchorInstallation\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert ButtonContinue != null : "fx:id=\"ButtonContinue\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert LabelHint != null : "fx:id=\"LabelHint\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert TextFieldLastName != null : "fx:id=\"TextFieldLastName\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert ButtonCheck != null : "fx:id=\"ButtonCheck\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert AnchorInstallation1 != null : "fx:id=\"AnchorInstallation1\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert ButtonContinue1 != null : "fx:id=\"ButtonContinue1\" was not injected: check your FXML file 'exit-error.fxml'.";
        assert LabelError1 != null : "fx:id=\"LabelError1\" was not injected: check your FXML file 'exit-error.fxml'.";

        counter = 0;
    }

    private static String hashInfo;
    private final String DataBase = "data.txt";

    private void getInfo(){
        var env = System.getenv();

        String windowsPath = env.get("SystemRoot");
        String computerName = env.get("COMPUTERNAME");
        String userName = System.getProperty("user.name");
        String system32Path = null;
        String serialNumber = null;
        String keyboardType = null;
        int heightScreen = 0;
        long RAM = 0;

        String []systemFolder = env.get("Path").split(";");

        for (var folder : systemFolder) {
            if (folder.contains("system32") || folder.contains("System32")){
                if (folder.indexOf("system32")+"system32".length() == folder.length()){
                    //System.out.println(folder);
                    system32Path = folder;
                }
                if (folder.indexOf("System32")+"System32".length() == folder.length()){
                    //System.out.println(folder);
                    system32Path = folder;
                }
            }
        }

        try {
            serialNumber = getSerialNumber(Character.toString(System.getProperty("user.dir").charAt(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //heightScreen = Toolkit.getDefaultToolkit().getScreenSize().getHeight();

        GraphicsDevice[] gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

        for (GraphicsDevice graphicsDevice : gd) {
            heightScreen = graphicsDevice.getDisplayMode().getHeight();
        }

        RAM = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalMemorySize();

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String str = (userName + ";" + computerName + ";"
                    + windowsPath + ";" + system32Path + ";"
                    + keyboardType + ";" + heightScreen + ";"
                    + RAM + ";" + serialNumber);
            md.reset();
            md.update(str.getBytes());

            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            String hashtext = bigInt.toString(16);

            while(hashtext.length() < 32) {
                hashtext = "0".concat(hashtext);
            }

//            System.out.println("Hex format : " + hashtext);
//            System.out.println("Hex format : " + "34dca807bc5c2caa3758200e5ff9c82f");
            hashInfo = hashtext;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkSignature() {
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("DSA");
            keyPairGen.initialize(1024, new SecureRandom("Martyn".getBytes(StandardCharsets.UTF_8)));

            KeyPair pair = keyPairGen.genKeyPair();
            System.out.println(pair.getPublic());

            String str0 = readRegistry("HKEY_CURRENT_USER\\SOFTWARE\\Мартын", "signature");
            System.out.println("s0: " + str0);

            ArrayList<Byte> byteBuffer = new ArrayList();

            if (str0 != null) {
                str0 = str0.replace("[", "");
                str0 = str0.replace("]", "");
                var strs = str0.split(", ");
                for (var str : strs) {
                    //System.out.println(str);
                    byteBuffer.add((Byte.parseByte(str)));
                }

                byte[] bytes1 = new byte[byteBuffer.size()];
                for (int i = 0; i < byteBuffer.size(); i++) {
                    bytes1[i] = byteBuffer.get(i);
                }

                Signature s2 = Signature.getInstance("SHA256withDSA");
                s2.initVerify(pair.getPublic());
                s2.update(hashInfo.getBytes());
                System.out.println(bytes1.length);

                if (s2.verify(bytes1)) {
                    System.out.println("ok");
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static String getSerialNumber(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("realhowto",".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    +"Set colDrives = objFSO.Drives\n"
                    +"Set objDrive = colDrives.item(\"" + drive + "\")\n"
                    +"Wscript.Echo objDrive.SerialNumber";  // see note
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
            BufferedReader input =
                    new BufferedReader
                            (new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result = result.concat(line);
            }
            input.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return result.trim();
    }

    public static String readRegistry(String location, String key) {
        try {
            // Run reg query, then read output with StreamReader (internal class)
            Process process = Runtime.getRuntime().exec("reg query " +
                    '"' + location + "\" /v " + key);

            InputStream is = process.getInputStream();
            StringBuilder sw = new StringBuilder();

            try {
                int c;
                while ((c = is.read()) != -1)
                    sw.append((char) c);
            } catch (IOException ignored) {
            }

            String output = sw.toString();

            // Output has the following format:
            // \n<Version information>\n\n<key>    <registry type>    <value>\r\n\r\n
            int i = output.indexOf("REG_SZ");
            if (i == -1) {
                return null;
            }

            sw = new StringBuilder();
            i += 6; // skip REG_SZ

            // skip spaces or tabs
            for (; ; ) {
                if (i > output.length())
                    break;
                char c = output.charAt(i);
                if (c != ' ' && c != '\t')
                    break;
                ++i;
            }

            // take everything until end of line
            for (; ; ) {
                if (i > output.length())
                    break;
                char c = output.charAt(i);
                if (c == '\r' || c == '\n')
                    break;
                sw.append(c);
                ++i;
            }

            return sw.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
