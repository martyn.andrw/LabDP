package app.lab6dpapp;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class AdminController {
    private static String passphrase;
    private int ind;
    private int size;
    private Controller.User localUser;
    private static ArrayList<Controller.User> ArrayUsers;

    public static void SetPass(String new_pass){
        passphrase = new_pass;
    }

    public static void SetAU(ArrayList<Controller.User> newArrayUsers){
        ArrayUsers = newArrayUsers;
    }


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label AdminLabel;

    @FXML
    private MenuBar AdminMenuBar;

    @FXML
    private Menu MainMenuA;

    @FXML
    private MenuItem SettingsA;

    @FXML
    private MenuItem AboutA;

    @FXML
    private Button NextButton;

    @FXML
    private Button AdminSave;

    @FXML
    private Button BackButton;

    @FXML
    private Button OnFirst;

    @FXML
    private Button LastButton;

    @FXML
    private Button BanButton;

    @FXML
    private Button NewButton;

    @FXML
    private Button RestrictionButton;

    @FXML
    private TextField LoginStatus;

    @FXML
    private TextField StatusStatus;

    @FXML
    private TextField PasswordStatus;

    @FXML
    void About(ActionEvent event) {
        try {
            Stage newAbout = MainApplication.startPage(new Stage(), "About.fxml", "About");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @FXML
    void OnSave(ActionEvent event) {
        /*decrypt file here*/
        FileEncryption fe = new FileEncryption(passphrase);
        fe.Decrypt("data.enc");
        File dataBase = new File("data.txt");

        try (FileWriter fileWriter = new FileWriter("data.txt")) {
            for (Controller.User arrayUser : ArrayUsers){
                String text = arrayUser.GetLogin() + " " + arrayUser.GetPassword() + " " + arrayUser.GetIntStatus() + " " + arrayUser.GetRestriction() + "\n";
                fileWriter.write(text);
            }
            fileWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }

        fe.Encrypt("data.txt", "data.enc");
        System.out.println("admin delete: ".concat(Boolean.toString(dataBase.delete())));
    }

    @FXML
    void OnActionAdminSettings(ActionEvent event){
        Stage stage = (Stage) AdminMenuBar.getScene().getWindow();
        stage.close();
        Stage stageUser = new Stage();
        try {
            MainApplication.startPage(stageUser, "main-page.fxml", "Main page");
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @FXML
    void OnBack(ActionEvent event) {
        AdminLabel.setText("");
        ind--;
        if (ind < 0){
            ind++;
        } else {
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        }
    }

    @FXML
    void OnBan(ActionEvent event) {
        AdminLabel.setText("");
        if (localUser.GetIntStatus() != 1){
            ArrayUsers.remove(ind);
            localUser.ChangeStatus(1);
            ArrayUsers.add(ind, localUser);
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
            AdminLabel.setAlignment(Pos.TOP_CENTER);
            AdminLabel.setTextFill(Paint.valueOf("GREEN"));
            AdminLabel.setText("Successfully blocked!");
        } else {
            ArrayUsers.remove(ind);
            localUser.ChangeStatus(10);
            ArrayUsers.add(ind, localUser);
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
            AdminLabel.setAlignment(Pos.TOP_CENTER);
            AdminLabel.setTextFill(Paint.valueOf("GREEN"));
            AdminLabel.setText("Successfully unblocked!");
        }
    }

    @FXML
    void OnFirst(ActionEvent event) {
        AdminLabel.setText("");
        if (ArrayUsers.size() > 0){
            ind = 0;
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        }
    }

    @FXML
    void OnLast(ActionEvent event) {
        AdminLabel.setText("");
        if (ArrayUsers.size() > 0){
            ind = ArrayUsers.size()-1;
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        }
    }

    boolean NewFlag;
    @FXML
    void OnNew(ActionEvent event) {
        AdminLabel.setText("");
        if (!NewFlag) {
            LoginStatus.setText("");
            StatusStatus.setText("");
            PasswordStatus.setText("");

            LoginStatus.setDisable(false);
            StatusStatus.setDisable(false);
            PasswordStatus.setDisable(false);
            NewFlag = !NewFlag;

            NextButton.setDisable(true);
            BackButton.setDisable(true);
            OnFirst.setDisable(true);
            LastButton.setDisable(true);
            BanButton.setDisable(true);
            RestrictionButton.setDisable(true);

            OnFirst.setText("Add");
        } else {
            String login = LoginStatus.getText();
            String status = StatusStatus.getText();
            String restr = PasswordStatus.getText();

            if (!login.equals("") && login.length() > 2) {
                boolean log = Controller.GetUserForAdmin(login);
                if (!log) {
                    if (status.equals("")) {
                        status = "10";
                    }
                    if (restr.equals("")) {
                        restr = "false";
                    }

                    if (!restr.equals("true") && !restr.equals("false")){
                        AdminLabel.setTextFill(Color.valueOf("RED"));
                        AdminLabel.setAlignment(Pos.TOP_CENTER);
                        AdminLabel.setText("Restriction might be <true> or <false>");
                    } else {
                        if (!status.equals("1") && !status.equals("10")) {
                            AdminLabel.setTextFill(Color.valueOf("RED"));
                            AdminLabel.setAlignment(Pos.TOP_CENTER);
                            AdminLabel.setText("Status might be <1>(banned) or <10>(Unbanned)");
                        } else {
                            Controller.User NewUsr;
                            NewUsr = new Controller.User(login, "", Integer.parseInt(status), Boolean.valueOf(restr));
                            ArrayUsers.add(NewUsr);
                            localUser = NewUsr;
                            ind = ArrayUsers.size() - 1;
                            NewFlag = !NewFlag;

                            LoginStatus.setDisable(true);
                            StatusStatus.setDisable(true);
                            PasswordStatus.setDisable(true);

                            NextButton.setDisable(false);
                            BackButton.setDisable(false);
                            OnFirst.setDisable(false);
                            LastButton.setDisable(false);
                            BanButton.setDisable(false);
                            RestrictionButton.setDisable(false);

                            OnFirst.setText("New");

                            AdminLabel.setTextFill(Color.valueOf("GREEN"));
                            AdminLabel.setAlignment(Pos.TOP_CENTER);
                            AdminLabel.setText("Successfully added! Click <Save> to save all changes");
                        }
                    }
                } else {
                    AdminLabel.setTextFill(Color.valueOf("RED"));
                    AdminLabel.setAlignment(Pos.TOP_CENTER);
                    AdminLabel.setText("Login must be unique!");
                }
            } else {
                AdminLabel.setTextFill(Color.valueOf("RED"));
                AdminLabel.setAlignment(Pos.TOP_CENTER);
                AdminLabel.setText("Login length cannot be less than 2 characters!");
            }
        }
    }

    @FXML
    void OnNext(ActionEvent event) {
        AdminLabel.setText("");
        ind++;
        if (ind >= ArrayUsers.size()){
            ind--;
        } else {
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        }
    }

    @FXML
    void OnRestriction(ActionEvent event) {
        AdminLabel.setText("");
        if (!localUser.GetRestriction()){
            ArrayUsers.remove(ind);
            localUser.ChangeRestriction(true);
            ArrayUsers.add(ind, localUser);
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        } else {
            ArrayUsers.remove(ind);
            localUser.ChangeRestriction(false);
            ArrayUsers.add(ind, localUser);
            localUser = ArrayUsers.get(ind);
            LoginStatus.setText(localUser.GetLogin());
            StatusStatus.setText(localUser.GetStatus());
            PasswordStatus.setText(String.valueOf(localUser.GetRestriction()));
        }
    }

    @FXML
    void initialize() {
        assert AdminMenuBar != null : "fx:id=\"AdminMenuBar\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert MainMenuA != null : "fx:id=\"MainMenuA\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert SettingsA != null : "fx:id=\"SettingsA\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert AboutA != null : "fx:id=\"AboutA\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert NextButton != null : "fx:id=\"NextButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert BackButton != null : "fx:id=\"BackButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert OnFirst != null : "fx:id=\"OnFirst\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert LastButton != null : "fx:id=\"LastButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert BanButton != null : "fx:id=\"BanButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert NewButton != null : "fx:id=\"NewButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert RestrictionButton != null : "fx:id=\"RestrictionButton\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert LoginStatus != null : "fx:id=\"LoginStatus\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert StatusStatus != null : "fx:id=\"StatusStatus\" was not injected: check your FXML file 'admin-page.fxml'.";
        assert PasswordStatus != null : "fx:id=\"PasswordStatus\" was not injected: check your FXML file 'admin-page.fxml'.";

        NewFlag = false;
        ind = 0;
    }
}
