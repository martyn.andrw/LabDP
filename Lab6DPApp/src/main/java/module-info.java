module app.lab6dpapp {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.management;
    requires java.desktop;
    requires jdk.management;
    requires javafx.base;


    opens app.lab6dpapp to javafx.fxml;
    exports app.lab6dpapp;
}