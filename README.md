LabDP
Запустите Lab6DP для установки приложения. Во время установки по указанному пути создается папка Lab6Martyn, в ней и находится установленное приложение
Сбор информации происходит в функции getInfo() в StartController.java
Хеширование информации - в HashInfo() в StartController.java
Создание и запись в реестр электронной подписи - в onActionNext() в StartController.java
StartController.java после установки находится в \Lab6Martyn\setup\src и в исходных файлах Lab6DP\src\main\java\com\lab6dp\lab6dp

За сбор, хеширование информации и проверку подписи в приложении отвечает файл EndOfTime.java по пути \Lab6Martyn\application\src либо Lab6DP\Lab6DPApp\src\main\java\app\lab6dpapp
В функции checkSignature() - проверка подписи из реестра
