package com.lab6dp.lab6dp;

import com.sun.management.OperatingSystemMXBean;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.Label;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.DSAPrivateKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

public class StartController {

    @FXML
    private AnchorPane AnchorInstallation;
    @FXML
    private AnchorPane AnchorInstallation11;
    @FXML
    private TextField TextFieldFileManager;
    @FXML
    private TextField TextFieldDone0;
    @FXML
    private TextField TextFieldDone1;
    @FXML
    private TextField TextFieldDone2;
    @FXML
    private Button ButtonFileManager;
    @FXML
    private Button ButtonContinue;
    @FXML
    private Button ButtonExit;
    @FXML
    private Label LabelError;
    @FXML
    private Label LabelDone;

    @FXML
    private AnchorPane AnchorInstallation1;
    @FXML
    private TextField TextFieldFileManager1;
    @FXML
    private Button ButtonContinue1;
    @FXML
    private javafx.scene.control.ProgressBar ProgressBar;

    private String directoryPath;
    private Boolean isDirectory;

    private String userName;
    private String computerName;
    private String windowsPath;
    private String system32Path;
    private String keyboardType;
    private String hashInfo;
    private String signature;
    private int heightScreen;
    private long RAM;
    private String serialNumber;

    @FXML
    void onActionDone(ActionEvent event) {
        AnchorInstallation.setVisible(false);
        AnchorInstallation1.setVisible(false);
        AnchorInstallation11.setVisible(true);

        TextFieldDone0.setText(directoryPath);
        TextFieldDone1.setText(showInfo());
        TextFieldDone2.setText("HKEY_CURRENT_USER\\SOFTWARE\\Мартын");
    }

    @FXML
    void onActionExit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void onActionTextField(ActionEvent event) {
    }

    @FXML
    void onActionFileManager(ActionEvent event) {
        LabelError.setText("");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        JFrame stage = new JFrame();
        fileChooser.showOpenDialog(stage);

        File file = fileChooser.getSelectedFile();
        if (file != null && file.isDirectory()) {
            //System.out.println(file);
            TextFieldFileManager.setText(file.getPath() + "\\Lab6Martyn");
            directoryPath = file.getPath();
            isDirectory = true;
        } else {
            TextFieldFileManager.setText("");
            directoryPath = "";
            isDirectory = false;
        }
        stage.dispose();
    }

    @FXML
    void onActionNext(ActionEvent event) {
        if (!isDirectory) {
            directoryPath = TextFieldFileManager.getText();
        }
        if (new File(directoryPath).isDirectory()) {
            if (directoryPath != null && !directoryPath.equals("")) {
                System.out.println(showInfo());
                AnchorInstallation.setVisible(false);
                AnchorInstallation1.setVisible(true);
                TextFieldFileManager1.setText(directoryPath + "\\Lab6Martyn");

                try {
                    hashInfo = HashInfo();
                    KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("DSA");
                    keyPairGen.initialize(1024, new SecureRandom("Martyn".getBytes(StandardCharsets.UTF_8)));

                    KeyPair pair = keyPairGen.genKeyPair();

                    /**/
                    Signature s = Signature.getInstance("SHA256withDSA");
                    s.initSign(pair.getPrivate());
                    //s.update(hashInfo.getBytes());
                    s.update(hashInfo.getBytes());
                    byte[] signatureValue = s.sign();
                    /**/

                    if (install()) {
                        String command0 = "REG ADD HKEY_CURRENT_USER\\SOFTWARE\\Мартын /v " + "signature" + " /d " + "\"" + Arrays.toString(signatureValue) + "\"";
                        System.out.println(command0);
                        Runtime.getRuntime().exec(command0);
                        String str0 = readRegistry("HKEY_CURRENT_USER\\SOFTWARE\\Мартын", "signature");

                        ArrayList<Byte> byteBuffer = new ArrayList();
                        if (str0 != null) {
                            byte[] bytes;
                            str0 = str0.replace("[", "");
                            str0 = str0.replace("]", "");
                            var strs = str0.split(", ");
                            for (var str : strs) {
                                //System.out.println(str);
                                byteBuffer.add((Byte.parseByte(str)));
                            }
//                        System.out.println("h: " + byteBuffer);

                            byte[] bytes1 = new byte[byteBuffer.size()];
                            for (int i = 0; i < byteBuffer.size(); i++) {
                                bytes1[i] = byteBuffer.get(i);
                            }

                            System.out.println("orig: " + Arrays.toString(signatureValue));
                            System.out.println("str0: " + str0);
                            System.out.println("byts: " + Arrays.toString(bytes1));

                            Signature s2 = Signature.getInstance("SHA256withDSA");
                            s2.initVerify(pair.getPublic());
                            s2.update(hashInfo.getBytes(StandardCharsets.UTF_8));

                            if (s2.verify(bytes1)) {
                                System.out.println("ok");
                                ProgressBar.setProgress(1);
                                AnchorInstallation.setVisible(false);
                                AnchorInstallation1.setVisible(true);

                                LabelDone.setText("Установка прошла успешно!");
                                LabelDone.setAlignment(Pos.TOP_CENTER);
                                LabelDone.setTextFill(Paint.valueOf("GREEN"));
                                ButtonContinue1.setDisable(false);
                            } else {
                                System.out.println("error");
                                LabelError.setText("Что-то пошло не так с выполнением reg add");
                                LabelError.setAlignment(Pos.TOP_CENTER);
                                LabelError.setTextFill(Paint.valueOf("RED"));
                                ButtonContinue1.setDisable(true);
                            }
                        }

                    } else {
                        AnchorInstallation.setVisible(true);
                        AnchorInstallation1.setVisible(false);
                        TextFieldFileManager1.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                LabelError.setTextAlignment(TextAlignment.CENTER);
                LabelError.setTextFill(Color.valueOf("RED"));
                LabelError.setText("Неправильный путь!");
            }
        } else {
            LabelError.setTextAlignment(TextAlignment.CENTER);
            LabelError.setTextFill(Color.valueOf("RED"));
            LabelError.setText("Неправильный путь!");
        }
    }

    @FXML
    void initialize() {
        assert AnchorInstallation != null : "fx:id=\"AnchorInstallation\" was not injected: check your FXML file 'installation.fxml'.";
        assert AnchorInstallation1 != null : "fx:id=\"AnchorInstallation\" was not injected: check your FXML file 'installation.fxml'.";
        assert AnchorInstallation11 != null : "fx:id=\"AnchorInstallation\" was not injected: check your FXML file 'installation.fxml'.";
        assert TextFieldFileManager != null : "fx:id=\"TextFieldFileManager\" was not injected: check your FXML file 'installation.fxml'.";
        assert TextFieldDone0 != null : "fx:id=\"TextFieldFileManager\" was not injected: check your FXML file 'installation.fxml'.";
        assert TextFieldDone1 != null : "fx:id=\"TextFieldFileManager\" was not injected: check your FXML file 'installation.fxml'.";
        assert TextFieldDone2 != null : "fx:id=\"TextFieldFileManager\" was not injected: check your FXML file 'installation.fxml'.";
        assert ButtonFileManager != null : "fx:id=\"ButtonFileManager\" was not injected: check your FXML file 'installation.fxml'.";
        assert ButtonContinue != null : "fx:id=\"ButtonContinue\" was not injected: check your FXML file 'installation.fxml'.";
        assert ButtonExit != null : "fx:id=\"ButtonContinue\" was not injected: check your FXML file 'installation.fxml'.";
        assert LabelError != null : "fx:id=\"LabelError\" was not injected: check your FXML file 'installation.fxml'.";
        assert LabelDone != null : "fx:id=\"LabelError\" was not injected: check your FXML file 'installation.fxml'.";

        isDirectory = false;
    }

    public static String readRegistry(String location, String key) {
        try {
            // Run reg query, then read output with StreamReader (internal class)
            Process process = Runtime.getRuntime().exec("reg query " +
                    '"' + location + "\" /v " + key);

            InputStream is = process.getInputStream();
            StringBuilder sw = new StringBuilder();

            try {
                int c;
                while ((c = is.read()) != -1)
                    sw.append((char) c);
            } catch (IOException e) {
            }

            String output = sw.toString();

            // Output has the following format:
            // \n<Version information>\n\n<key>    <registry type>    <value>\r\n\r\n
            int i = output.indexOf("REG_SZ");
            if (i == -1) {
                return null;
            }

            sw = new StringBuilder();
            i += 6; // skip REG_SZ

            // skip spaces or tabs
            for (; ; ) {
                if (i > output.length())
                    break;
                char c = output.charAt(i);
                if (c != ' ' && c != '\t')
                    break;
                ++i;
            }

            // take everything until end of line
            for (; ; ) {
                if (i > output.length())
                    break;
                char c = output.charAt(i);
                if (c == '\r' || c == '\n')
                    break;
                sw.append(c);
                ++i;
            }

            return sw.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private void getInfo(){
        var env = System.getenv();

        windowsPath = env.get("SystemRoot");
        computerName = env.get("COMPUTERNAME");
        userName = System.getProperty("user.name");

        String []systemFolder = env.get("Path").split(";");

        for (var folder : systemFolder) {
            if (folder.contains("system32") || folder.contains("System32")){
                if (folder.indexOf("system32")+"system32".length() == folder.length()){
                    //System.out.println(folder);
                    system32Path = folder;
                }
                if (folder.indexOf("System32")+"System32".length() == folder.length()){
                    //System.out.println(folder);
                    system32Path = folder;
                }
            }
        }

        try {
            serialNumber = getSerialNumber(Character.toString(System.getProperty("user.dir").charAt(0)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        GraphicsDevice[] gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

        for (GraphicsDevice graphicsDevice : gd) {
            heightScreen = graphicsDevice.getDisplayMode().getHeight();
        }

        RAM = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalMemorySize();
    }

    public String showInfo(){
        getInfo();
        return (userName + ";" + computerName + ";"
                + windowsPath + ";" + system32Path + ";"
                + keyboardType + ";" + Integer.toString(heightScreen) + ";"
                + Long.toString(RAM) + ";" + serialNumber);
    }

    private String HashInfo(){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String str = showInfo();
            md.reset();
            md.update(str.getBytes());

            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            String hashtext = bigInt.toString(16);

            while(hashtext.length() < 32) {
                hashtext = "0".concat(hashtext);
            }

            return hashtext;
            //System.out.println("Hex format : " + hashtext);
            //System.out.println("Hex format : " + "34dca807bc5c2caa3758200e5ff9c82f");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean install() {
        try {
//            setup
            String dir = System.getProperty("user.dir");
            String folder_to_move = directoryPath;
            if (!directoryPath.contains("\\Lab6Martyn")) {
                folder_to_move = directoryPath + "\\Lab6Martyn";
            }
            File dirForFiles = new File(folder_to_move + "\\setup\\src\\");
            File dirFiles = new File(dir + "\\src\\main\\java\\com\\lab6dp\\lab6dp");

//            setup src
            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                for (File fil : Objects.requireNonNull(dirFiles.listFiles())){
                    if (fil.isFile()) {
                        //System.out.println(fil.getName());

                        Path fileToMovePath = Paths.get(dirFiles + "\\" + fil.getName());
                        Path targetPath = Paths.get(folder_to_move + "\\setup\\src\\" + fil.getName());
                        Files.copy(fileToMovePath, targetPath, REPLACE_EXISTING);
                    }
                }
                ProgressBar.setProgress(0.15);
            }

//            setup resources
            dirForFiles = new File(folder_to_move + "\\setup\\resources\\");
            dirFiles = new File(dir + "\\src\\main\\resources\\com\\lab6dp\\lab6dp");

            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                for (File fil : Objects.requireNonNull(dirFiles.listFiles())) {
                    if (fil.isFile()) {
                        //System.out.println(fil.getName());

                        Path fileToMovePath = Paths.get(dirFiles + "\\" + fil.getName());
                        Path targetPath = Paths.get(folder_to_move + "\\setup\\resources\\" + fil.getName());
                        Files.copy(fileToMovePath, targetPath, REPLACE_EXISTING);
                    }
                }
                ProgressBar.setProgress(ProgressBar.getProgress() + 0.15);
            }

//                setup jar
            dirForFiles = new File(folder_to_move + "\\setup\\");
            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                //Files.copy(Paths.get(dir + "\\Lab6DP.jar"), Paths.get(folder_to_move + "\\setup\\setup.jar"), REPLACE_EXISTING);
                ProgressBar.setProgress(ProgressBar.getProgress() + 0.15);
            }

//            app src
            dirForFiles = new File(folder_to_move + "\\application\\src\\");
            dirFiles = new File(dir + "\\Lab6DPApp\\src\\main\\java\\app\\lab6dpapp");

            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                for (File fil : Objects.requireNonNull(dirFiles.listFiles())){
                    if (fil.isFile()) {
                        //System.out.println(fil.getName());

                        Path fileToMovePath = Paths.get(dirFiles + "\\" + fil.getName());
                        Path targetPath = Paths.get(folder_to_move + "\\application\\src\\" + fil.getName());
                        Files.copy(fileToMovePath, targetPath, REPLACE_EXISTING);
                    }
                }
                ProgressBar.setProgress(ProgressBar.getProgress() + 0.15);
            }

//            app resources
            dirForFiles = new File(folder_to_move + "\\application\\resources\\");
            dirFiles = new File(dir + "\\Lab6DPApp\\src\\main\\resources\\app\\lab6dpapp");

            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                for (File fil : Objects.requireNonNull(dirFiles.listFiles())) {
                    if (fil.isFile()) {
                        //System.out.println(fil.getName());

                        Path fileToMovePath = Paths.get(dirFiles + "\\" + fil.getName());
                        Path targetPath = Paths.get(folder_to_move + "\\application\\resources\\" + fil.getName());
                        Files.copy(fileToMovePath, targetPath, REPLACE_EXISTING);
                    }
                }
                ProgressBar.setProgress(ProgressBar.getProgress() + 0.15);
            }

//                app jar
            dirForFiles = new File(folder_to_move + "\\application\\");
            if (dirForFiles.mkdirs() || dirForFiles.isDirectory()) {
                Files.copy(Paths.get(dir + "\\Lab6DPApp\\Lab6DPApp.jar"), Paths.get(folder_to_move + "\\Application.jar"), REPLACE_EXISTING);
                ProgressBar.setProgress(ProgressBar.getProgress() + 0.15);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static String getSerialNumber(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("realhowto",".vbs");
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    +"Set colDrives = objFSO.Drives\n"
                    +"Set objDrive = colDrives.item(\"" + drive + "\")\n"
                    +"Wscript.Echo objDrive.SerialNumber";  // see note
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
            BufferedReader input =
                    new BufferedReader
                            (new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result = result.concat(line);
            }
            input.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return result.trim();
    }
}
