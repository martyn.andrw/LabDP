module com.lab6dp.lab6dp {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;
    requires java.desktop;
    requires java.compiler;
    requires jdk.management;
    requires java.prefs;

    opens com.lab6dp.lab6dp to javafx.fxml;
    exports com.lab6dp.lab6dp;
}